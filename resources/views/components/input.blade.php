@props(['type' => '', 'name' => '', 'options', 'model' => '', 'label' => ''])

@if (in_array($type, ['text', 'date']))
    <div class="grid grid-cols-8">
        <label for="{{$name}}" class="col-span-2 text-center font-bold  @error($name) text-red-600 border-red-600 @enderror">{{$label}}</label>
        <input id="{{$name}}" type="{{$type}}" name="{{$name}}" value="{{ $model->$name ?? old($name)}}"
            class="border border-gray-500 rounded col-span-5 p-1 text-center">
    </div>
@elseif ($type == 'select')
    @php($options = explode(',', $options))
    <div class="grid grid-cols-8">
        <label for="{{$name}}" class="col-span-2 text-center font-bold @error($name) text-red-600 border-red-600 @enderror">{{$label}}</label>
        <select name="{{$name}}" id="{{$name}}" class="border border-gray-500 rounded col-span-5 p-1 text-center">
            @foreach ($options as $option)
                @if ($model)
                    <option class="text-center p-1" value="{{$option}}" @if($option == $model->$name)selected @endif>{{$option}}</option>
                @else
                    <option class="text-center p-1" value="{{$option}}" @if($option == old($name))selected @endif>{{$option}}</option>
                @endif
            @endforeach
        </select>
    </div>
@elseif ($type == 'submit')
    <div class="grid grid-cols-8">
        <input type="{{$type}}" value="{{$label}}"
            class="border border-black rounded-full hover:bg-black hover:text-white hover:cursor-pointer col-span-3 col-start-4 p-1">
    </div>
@elseif ($type == 'textarea')
    <div class="grid grid-cols-8">
        <label for="{{$name}}" class="col-span-2 text-center font-bold @error($name) text-red-600 border-red-600 @enderror">{{$label}}</label>
        <textarea name="{{$name}}" id="{{$name}}" rows="5"
        class="border border-gray-500 rounded col-span-5 p-2">{{ $model->$name ?? old($name)}}</textarea>
    </div>
@endif
@error($name)
    <div class="grid grid-cols-8">
        <div class="text-center text-red-600 text-sm col-span-5 col-start-3">{{$message}}</div>
    </div>
@enderror

