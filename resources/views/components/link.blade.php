<a href="{{$href}}"
    class="bg-black px-2 py-1 rounded text-white hover:bg-gray-900">
    {{$text}}
</a>
