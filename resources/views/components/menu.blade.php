<div x-data="{menu: false}" class="fixed bottom-5 left-5 z-10">
    <div x-show="menu" class="bg-white w-64 border-2 border-black outline outline-gray-200 mb-2 p-4 rounded-lg">
        <div x-on:click="menu = false" class="grid gap-2">
            @if ($scope == "projects")
                <a x-on:click="createProject = true" class="hover:cursor-pointer">Projekt erstellen</a>
            @elseif($scope == "packages")
                <a x-on:click="editProject = true, createPackage = false" class="hover:cursor-pointer">Projekt bearbeiten</a>
                <a x-on:click="createPackage = true, editProject = false" class="hover:cursor-pointer">Arbeitspaket erstellen</a>
                <form class="m-0" method="POST" action="{{route('project.destroy', [$model])}}">
                    @csrf
                    @method('DELETE')
                    <input class="hover:cursor-pointer" type="submit" value="Projekt entfernen">
                </form>
            @elseif($scope == "package")
                <a x-on:click="editPackage = true" class="hover:cursor-pointer">Arbeitspaket bearbeiten</a>
                <form class="m-0" method="POST" action="{{route('package.destroy', [$model])}}">
                    @csrf
                    @method('DELETE')
                    <input class="hover:cursor-pointer" type="submit" value="Arbeitspaket entfernen">
                </form>
            @endif
            <a href="/">Einstellungen</a>
            <a href="/">Logout</a>
        </div>
    </div>
    <button x-on:click="menu = !menu" class="bg-white p-4 border-2 border-black outline outline-2 outline-gray-200 rounded-full hover:bg-black hover:text-white">
        Menu
    </button>
</div>
