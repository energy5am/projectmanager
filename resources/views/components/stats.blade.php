@props(['title' => '', 'content' => ''])

@empty(!$content)
    <div class="bg-white border-2 border-black relative px-4 pt-6 pb-2 rounded-lg">
        <div class="absolute rounded -top-2 -left-1 border-2 font-bold border-black px-2 text-sm bg-white">{{$title}}</div>
        <div class="">{{$content}}</div>
    </div>
@endempty
