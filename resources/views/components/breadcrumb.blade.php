@isset($url)
    > <a {{$attributes}} href="{{url($url)}}">{{$name}}</a>
@else
    <a {{$attributes}} href="{{url('/')}}">Home</a>
@endisset
