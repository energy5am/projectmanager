<x-layout>
    <div @if(session('editProject')) x-data="{editProject: true}" @elseif(session('createPackage')) x-data="{createPackage: true}" @else x-data="{editProject: false, createPackage: false}" @endif class="">
        <x-menu scope="packages" :model="$project"/>
        <div class="grid grid-cols-5">
            <div class="col-span-3 h-screen overflow-scroll">
                <div class="p-4 sticky top-0 bg-white">
                    <x-breadcrumb />
                    <x-breadcrumb url="{{ route('project.show', [$project]) }}" name="{{ $project->title }}" class="font-bold"/>
                </div>
                <div x-show="!editProject && !createPackage" class="grid gap-4 grid-cols-3 p-4">
                    @foreach ($packages->sortBy('deadline')->sortByDesc('status') as $package)
                        <div class="text-{{$package->getStatusColor($package)}} border-2 border-{{$package->getStatusColor($package)}} bg-white p-4 grid gap-2 text-center rounded-lg">
                            <div class="font-bold">{{ $package->title }}</div>
                            <div class="">{{ $package->shortDescription }}</div>
                            <div class="">{{ $package->duration }}</div>
                            <div class="">{{ $package->deadline }}</div>
                            <a href="{{ route('package.show', [$package]) }}"
                                class="bg-{{$package->getStatusColor($package)}} px-2 py-1 rounded text-white hover:opacity-80">
                                Anzeigen
                            </a>
                        </div>
                    @endforeach
                </div>
                <div x-show="editProject" class="w-full">
                    <form class="grid gap-4 p-4" method="POST" action="{{route('project.update', [$project])}}">
                        @csrf
                        @method('PATCH')
                        <x-input label="Titel*" name="title" type="text" :model="$project"/>
                        <x-input label="Status*" name="status" type="select" options="Offen,Fertig,Abgebrochen" :model="$project"/>
                        <x-input label="Kosten*" name="costs" type="textarea" :model="$project"/>
                        <x-input label="Nutzen*" name="benefit" type="textarea" :model="$project"/>
                        <x-input label="Umfang*" name="scope" type="textarea" :model="$project"/>
                        <x-input label="Qualität*" name="quality" type="textarea" :model="$project"/>
                        <x-input label="Risiko*" name="risk" type="textarea" :model="$project"/>
                        <x-input label="Interessengruppen" name="groups" type="textarea" :model="$project"/>
                        <x-input label="Deadline*" name="deadline" type="date" :model="$project"/>
                        <x-input label="Speichern" name="speichern" type="submit"/>
                    </form>
                    <div class="grid grid-cols-8 px-4">
                        <button x-on:click="editProject = false"
                            class="border border-black rounded-full hover:bg-black hover:text-white hover:cursor-pointer col-span-3 col-start-4 p-1">
                            Abbrechen
                        </button>
                    </div>
                </div>
                <div x-show="createPackage" class="w-full">
                    <form class="grid gap-4 p-4" method="POST" action="{{route('package.store')}}">
                        @csrf
                        <x-input label="Titel*" name="title" type="text"/>
                        <x-input label="Zuweisung*" name="assigment" type="text"/>
                        <x-input label="Status" name="status" type="select" options="Offen,Fertig"/>
                        <x-input label="Duration" name="duration" type="select" options="~Stunde,~Tag,~Woche"/>
                        <x-input label="Kurze Beschreibung" name="shortDescription" type="textarea"/>
                        <x-input label="Beschreibung" name="description" type="textarea"/>
                        <x-input label="Deadline*" name="deadline" type="date"/>
                        <input type="hidden" name="project_id" value="{{$project->id}}">
                        <x-input label="Erstellen" name="erstellen" type="submit"/>
                    </form>
                    <div class="grid grid-cols-8 px-4">
                        <button x-on:click="createPackage = false"
                            class="border border-black rounded-full hover:bg-black hover:text-white hover:cursor-pointer col-span-3 col-start-4 p-1">
                            Abbrechen
                        </button>
                    </div>
                </div>
            </div>
            <div class="bg-gray-100 col-span-2 h-screen">
                <div class="p-4 grid gap-4">
                    <h1 class="text-2xl text-center font-black">Projekt</h1>
                    <x-stats title="Titel" content="{{$project->title}}"/>
                    <x-stats title="Fortschritt" content="{{$project->getProgress($project)}}%"/>
                    <x-stats title="Nutzen" content="{{$project->benefit}}"/>
                    <x-stats title="Deadline" content="{{$project->formatDeadline()}}"/>
                    <x-stats title="Umfang" content="{{$project->scope}}"/>
                    <x-stats title="Qualität" content="{{$project->quality}}"/>
                    <x-stats title="Risiko" content="{{$project->risk}}"/>
                    <x-stats title="Interessengruppen" content="{{$project->interest_group}}"/>
                    <x-stats title="Status" content="{{$project->status}}"/>
                </div>
            </div>
        </div>
    </div>
</x-layout>
