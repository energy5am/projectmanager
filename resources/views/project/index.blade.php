<x-layout>
    <div @if(session('createProject')) x-data="{createProject: true}" @else x-data="{createProject: false}" @endif>
        <x-menu scope="projects"/>
        <div class="grid grid-cols-5">
            <div class="col-span-4 h-screen overflow-scroll">
                <div class="bg-white row-span-1 p-4 sticky top-0">
                    <x-breadcrumb class="font-bold"/>
                </div>
                <div class="row-span-2">
                    <div x-show="!createProject" class="grid gap-4 grid-cols-5 p-4">
                        @foreach ($projects as $project)
                            <div class="text-{{$project->getStatusColor($project)}} border-2 border-{{$project->getStatusColor($project)}}
                                    bg-white grid gap-4 p-4 text-center rounded-lg">
                                <div class="font-bold">{{ $project->title }}</div>
                                <div class="bg-gray-100 rounded-lg h-4">
                                    <div class="bg-green-600 h-4 rounded-lg text-white" style="width: {{ $project->getProgress() }}%">
                                        <span class="text-xs">{{ $project->getProgress() }}%</span>
                                    </div>
                                </div>
                                <div class="">{{$project->formatDeadline()}}</div>
                                <a href="{{ route('project.show', [$project]) }}"
                                    class="bg-{{$project->getStatusColor($project)}} px-2 py-1 rounded text-white hover:opacity-80">
                                    Anzeigen
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div x-show="createProject" class="w-full">
                        <form class="grid gap-4" method="POST" action="{{route('project.store')}}">
                            @csrf
                            <x-input label="Titel*" name="title" type="text"/>
                            <x-input label="Kosten*" name="costs" type="textarea"/>
                            <x-input label="Nutzen*" name="benefit" type="textarea"/>
                            <x-input label="Umfang*" name="scope" type="textarea"/>
                            <x-input label="Qualität*" name="quality" type="textarea"/>
                            <x-input label="Risiko*" name="risk" type="textarea"/>
                            <x-input label="Interessengruppen" name="groups" type="textarea"/>
                            <x-input label="Deadline*" name="deadline" type="date"/>
                            <x-input label="Erstellen" name="erstellen" type="submit"/>
                        </form>
                        <div class="grid grid-cols-8">
                            <button x-on:click="createProject = false"
                                class="border border-black rounded-full hover:bg-black hover:text-white hover:cursor-pointer col-span-3 col-start-4 p-1">
                                Abbrechen
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-span-1 bg-gray-100 h-screen">
                <div class="">
                    <div class="p-4">Aktivitäten</div>
                </div>
            </div>
        </div>
    </div>
</x-layout>
