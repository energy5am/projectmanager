<x-layout>
    <div @if(session('editPackage')) x-data="{editPackage: true}" @else x-data="{editPackage: false}" @endif class="">
        <x-menu scope="package" :model="$package"/>
        <div class="grid grid-cols-5">
            <div class="col-span-3 h-screen overflow-scroll">
                <div class="bg-white row-span-1 p-4 sticky top-0">
                    <x-breadcrumb />
                    <x-breadcrumb url="{{ route('project.show', [$project]) }}" name="{{ $project->title }}"/>
                    <x-breadcrumb url="{{ route('package.show', [$package]) }}" name="{{ $package->title }}" class="font-bold"/>
                </div>
                <div class="row-span-2">
                    <div x-show="!editPackage" class="grid gap-4 grid-cols-5 p-4">
                        Kommentare
                    </div>
                </div>
                <div x-show="editPackage" class="w-full">
                    <form class="grid gap-4 p-4" method="POST" action="{{route('package.update', [$package])}}">
                        @csrf
                        @method('PATCH')
                        <x-input label="Titel*" name="title" type="text" :model="$package"/>
                        <x-input label="Zuweisung*" name="assigment" type="text" :model="$package"/>
                        <x-input label="Status" name="status" type="select" options="Offen,Fertig"/>
                        <x-input label="Dauer" name="duration" type="select" options="~Stunde,~Tag,~Woche" :model="$package"/>
                        <input type="hidden" name="project_id" value="{{$project->id}}">
                        <x-input label="Kurze Beschreibung" name="shortDescription" type="textarea" :model="$package"/>
                        <x-input label="Beschreibung" name="description" type="textarea" :model="$package"/>
                        <x-input label="Deadline*" name="deadline" type="date" :model="$package"/>
                        <x-input label="Speichern" name="speichern" type="submit"/>
                    </form>
                    <div class="grid grid-cols-8 px-4">
                        <button x-on:click="editPackage = false"
                            class="border border-black rounded-full hover:bg-black hover:text-white hover:cursor-pointer col-span-3 col-start-4 p-1">
                            Abbrechen
                        </button>
                    </div>
                </div>
            </div>
            <div class="bg-gray-100 col-span-2 h-screen">
                <div class="p-4 grid gap-4">
                    <h1 class="text-2xl text-center font-black">Arbeitspaket</h1>
                    <x-stats title="Titel" content="{{$package->title}}"/>
                    <x-stats title="Status" content="{{$package->status}}"/>
                    <x-stats title="Dauer" content="{{$package->duration}}"/>
                    <x-stats title="Kurze Beschreibung" content="{{$package->shortDescription}}"/>
                    <x-stats title="Beschreibung" content="{{$package->description}}"/>
                    <x-stats title="Zuweisung" content="{{$package->assigment}}"/>
                    <x-stats title="Deadline" content="{{$package->deadline}}"/>
                </div>
            </div>
        </div>
    </div>
</x-layout>
