<?php

use App\Http\Controllers\PackageController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return redirect()->route('project.index');
});

Route::resource('project', ProjectController::class);
Route::resource('package', PackageController::class);

