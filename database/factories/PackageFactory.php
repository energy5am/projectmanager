<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Package>
 */
class PackageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->text(20),
            'shortDescription' => fake()->text(100),
            'description' => fake()->text(500),
            'duration' => fake()->randomElement(['~Stunde', '~Tag', '~Woche']),
            'assigment' => fake()->name,
            'status' => fake()->randomElement(['Offen', 'Fertig']),
            'deadline' => fake()->dateTimeBetween(now(), '+2 months')
        ];
    }
}
