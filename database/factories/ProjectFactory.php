<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(2),
            'costs' => fake()->text(100),
            'scope' => fake()->text(100),
            'quality' => fake()->text(100),
            'benefit' => fake()->text(100),
            'risk' => fake()->text(100),
            'groups' => fake()->text(100),
            'status' => fake()->randomElement(['Offen', 'Fertig', 'Abgebrochen']),
            'deadline' => fake()->dateTimeBetween(now(), '+2 months')
        ];
    }
}
