<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Project;

class Package extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'assigment', 'duration', 'shortDescription', 'description', 'deadline', 'project_id', 'status'];

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function getStatusColor(Package $package){
        switch($package->status){
            case('Offen'): return 'black'; break;
            case('Fertig'): return 'green-600'; break;
        }
    }
}
