<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Package;
use DateTime;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'costs', 'scope', 'quality', 'benefit', 'risk', 'groups', 'status', 'deadline'];

    public function getProgress(){
        $allPackages = $this->packages()->count();
        if($allPackages == 0){ return 0; }
        $donePackages = $this->packages()->where('status', 'Fertig')->count();
        return round($donePackages / $allPackages, 2)*100;
    }

    public function packages(){
        return $this->hasMany(Package::class);
    }

    public function getStatusColor(Project $project){
        switch($project->status){
            case('Offen'): return 'black'; break;
            case('Fertig'): return 'green-600'; break;
            case('Abgebrochen'): return 'gray-600'; break;
        }
    }

    public function formatDeadline(){
        return (new DateTime($this->deadline))->format('d.m.Y');
    }
}
