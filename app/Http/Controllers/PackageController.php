<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePackageRequest;
use App\Http\Requests\UpdatePackageRequest;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'project_id' => 'required',
            'assigment' => 'required',
            'duration' => 'required',
            'shortDescription' => 'nullable',
            'description' => 'nullable',
            'deadline' => 'required|date'
        ]);

        if($validator->fails()){
            redirect()->route('package.index')->withErrors($validator)->with('createPackage', true);
        }

        $package = Package::create($validator->validated());

        return redirect()->route('package.show', [$package]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Package $package)
    {
        return view('package.show')->with('package', $package)->with('project', $package->project()->first());
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Package $package)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'status' => 'required',
            'project_id' => 'required',
            'assigment' => 'required',
            'duration' => 'required',
            'shortDescription' => 'nullable',
            'description' => 'nullable',
            'deadline' => 'required|date'
        ]);

        if($validator->fails()){
            redirect()->route('package.show', [$package])->withErrors($validator)->with('editPackage', true);
        }
        $package->update($validator->validated());

        return redirect()->route('package.show', [$package]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Package $package)
    {
        $project = $package->project()->first();
        $package->delete();
        return redirect()->route('project.show', [$project]);
    }
}
