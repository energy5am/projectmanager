<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('project.index')
            ->with('projects', Project::orderBy('status', 'DESC')->orderBy('deadline', 'ASC')->get());
    }
#    sortBy('deadline')->sortByDesc('status')
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'costs' => 'required',
            'scope' => 'required',
            'quality' => 'required',
            'benefit' => 'required',
            'risk' => 'required',
            'groups' => 'nullable',
            'status' => 'nullable',
            'deadline' => 'required|date'
        ]);

        if($validator->fails()){
            redirect()->route('project.index')->withErrors($validator)->with('createProject', true);
        }

        Project::create($validator->validated());

        return redirect()->route('project.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        return view('project.show')
            ->with('project', $project)
            ->with('packages', $project->packages()->orderBy('status', 'DESC')->orderBy('deadline', 'ASC')->get());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'costs' => 'required',
            'scope' => 'required',
            'quality' => 'required',
            'benefit' => 'required',
            'risk' => 'required',
            'groups' => 'nullable',
            'status' => 'nullable',
            'deadline' => 'required|date'
        ]);

        if($validator->fails()){
            redirect()->route('project.show', [$project])->withErrors($validator)->with('editProject', true);
        }
        $project->update($validator->validated());

        return redirect()->route('project.show', [$project]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('project.index');
    }
}
